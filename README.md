# tower-crane

Tooling for interacting with our infra and development.

Most tools are installed by running the appropriate script in this repo.
Tools that are required but not installed by the scripts are linked below.

For Linux, we support Ubuntu. Merge Requests to support other distros is welcome.

## Prerequisites

* Mac users should have [homebrew[(https://brew.sh)] installed.
* Linux users should have Python 3 and [pip](https://pypi.org/project/pip/) installed.

### Containers

* docker: Installing docker is complex and has different instructions by both OS and Linux Distro, please visit https://docs.docker.com/engine/install/ to install it. The scripts require docker to be installed, so do this before running them.

## What gets installed

* pack - buildpack cli
* docker-compose
* postgres - as a docker container matching our supported version, as well as the cli
* redis - as a docker container matching our supported version
* nvm - node version manager, via nvm's recommended install script

### Additional staff installations

* azctl - azure cloud cli
* kubectl - kubernetes cli
* linkerd cli - for interacting with our service mesh
* argo cli - interactin with our deployment platform

## Versions

We try to keep these scripts up to date, but most things 
have a version option you can optionally pass via environment variables.
For example, if you want a specific version of `pack` you can run the script like

```shell
$ PACK_VERSION=v0.27.0 ./community_setup_linux.sh
```
